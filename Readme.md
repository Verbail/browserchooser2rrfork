## Getting started (installation and setup)
(If something confuses you in here please let me know via Issues so I can change the text here)

1. Copy contents of highest numbered zip file folder from the Downloads section to a folder of your choice, such as "C:\Program Files (manual)\Browser Chooser 2\"
2. Run program. It will detect your browser(s) for you on first run.
3. Assuming you want to choose your browser when you click on a link you'll then want to
    1. Go to Options (see below), click on "Windows Default"
    2. Then  cick the "Add to Default Programs" button.
    3. Finally click "Show Defaults Dialog*" and in there choose Browser Chooser 2 after clicking under Web browser.


3. Optional: Set up a default browser and time 
    1. Go to Options (press "O" or click button top right) and you'll go to "Common\Browsers & Applications" as default. Choose a browser from the list (single click not double) then click "Set default"
    2. If you want to change the time before the default browser is launched, go to the "Settings\Startup" in options.
    2. Click "Save"
4. Optional: Set up some keyboard shortcuts.
    1. In that same browsers page (see above), double click a browser to edit it.
    2. Type a numbered hotkey (0-9). Fun fact: You can repeat a number to have multiple browsers open at one time - handy for testing / comparing
5. Optional: Set up some auto URLs. Go to (press "O" or click button top right) and click on "Common\Auto URLs"
    1. Add a URL by clicking... "Add" (surprise! :) ).
    2. In pop-up, type something for the URL or paste from a program of your choice. Choose a Browser and if you want it to be automatic (no delay) change Delay to 0
    3. Click "OK", then "Save". Fun extra: try using * at the end of a domain to match all URLs within that domain.


## Release notes:

### 2.6 (2020-09) - The explicit default release!

To make things a bit simpler you can now simply and explicitly set a browser up to be the default in the Browsers list, without having to use a catch-all URL to do this.
Don't worry though if you're coming from another release: That URL catch all (when the URL is set to "\*") still works and in fact overrides this *explicit* default browser - so if you're upgrading you might want to delete that "\*" URL entry then you never have to worry about putting it in the bottom of the URL list! Phew 😉

**Options tweaks:**

- Set the default browser in the Browsers & applications list by selecting a row (single click!) then clicking the new "Set default" button
- Show the default browser in the options browsers list too. Checkbox-tastic!

**Main window**

- Default - what is the default? Show this in the main window with a "D". This may only show if a hotkey is set - but is that a problem?

**Known issues**

- This isn't a big problem and it's fixed in code already but on the Downloads 2.6 release you won't see the default marker (D) on the main screen if that default browser doesn't have a hotkey. The default *works* though.
- And there's a workaround for the other thing you might notice; you can't remove the default at the moment. The workaround if you need it is to clone a browser, set that as default then delete that clone.

### 2.5 (2020-08)

**Main window**

- Show keyboard shortcuts (hotkeys) on the main window: it's not just me who forgets which button on my keyboard will start which browser or application, is it?
- Adding Space to pause the countdown. Now you can have a 2 second countdown knowing you can pause it just by hammering your Space key. Showing this in the UI too.
- Check for URL (if none, don't enable countdown - useful for running in debug)
- A bunch of small nip-and-tuck UI tweaks (small things like alignment tweaks to UI controls, trying to make some of the English messages less ambiguous)

**Options tweaks:**

- Show the timeout in the URLs list in Options. We have the space and it's handy to see it right there so there it is.
- Show the hotkey in the Browsers list in Options.
- Changed some of the buttons in options to be context-sensitive (e.g. only enable delete from URLs and Browsers lists if there's something selected).
- Allowing protocol and file type editors to be resized, and starting them off bigger.
- Warnings that URLs are case-sensitive (this behaviour will probably be changed later. Can't see how useful that is)
- Let folks know they can double click in the URL and Browser lists to edit those rows.
- Changing some defaults: Aero is out (for me without this this Edge's icon was hidden), Portable mode is on now (this may or may not be a good thing. Discuss) and Browser Chooser is a bit wider to start with
- Expand the options tree view by default (Save your poor clicking finger: I was just having to expand it each time, and it fits),

![Browser Chooser 2.6](Docs/2.6_release_screenshot.png)

## Notes for contributors:
(these are WiP too... please contact me and I'll help you and tidy these up too when I am reminded what needs to be set up!)

1. Clone code

2. Open sln file Browser Chooser 2 (don't just open the folder. The build selector dropdown should say things like just "Debug" not "Debug|Any" and "Debug|x64"
    1. Another clue you've opened via folder - it will ask you to select startup item.

3. I had all sort of issues with Browser Chooser 2.XmlSerializers.dll.
- I had to copy it from the original project.

## More urgent TODOs

fix browser choice (not sure what I meant here now(!))
look at app opening screens - put some "getting started" screens in for first run?

update start docs - help first timer (these, and wiki?).
 Set up browsers (shortcuts, timer, default. Should auto detect Opera too)
 Set up some auto URLs - even an example one or two
